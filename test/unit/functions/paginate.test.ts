import PaginationOptions from '../../../src/interfaces/PaginationOptions.interface';
import { paginate, paginateMany, paginateRawMany } from '../../../src/functions/paginate';
import { Repository, SelectQueryBuilder } from 'typeorm';
import { faker } from '@faker-js/faker';
import MetaOptions from 'src/interfaces/MetaOptions.interface';
import { PAGINATION } from '../../../src/constants/index';

describe('Functions', () => {
  describe('Paginate', () => {
    let mockRepository: Repository<any>;
    let mockQueryBuilder: SelectQueryBuilder<any>;
    let mockPaginate: PaginationOptions;
    let mockMeta: MetaOptions;
    let options;

    let confMaxAndMin = {
      min: 1,
      max: 100,
    };
    let page = faker.datatype.number(confMaxAndMin);
    let perPage = faker.datatype.number(confMaxAndMin);

    describe('paginateMany()', () => {
      beforeEach(() => {
        mockQueryBuilder = {
          getMany: jest.fn(),
          getCount: jest.fn(),
          take: jest.fn().mockReturnThis(),
          skip: jest.fn().mockReturnThis(),
          orderBy: jest.fn().mockReturnThis(),
        } as any;

        mockPaginate = { page, perPage };
        options = {};
      });

      it('should return an pagination if set all values ', async () => {
        mockMeta = { total: 0, perPage, pages: page, currentPage: 0 };
        jest.spyOn(mockQueryBuilder, 'getMany').mockResolvedValueOnce([]);
        jest.spyOn(mockQueryBuilder, 'getCount').mockResolvedValueOnce(0);
        let { items, meta } = await paginateMany(mockQueryBuilder, mockPaginate, 'something');

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });

      it('should return an pagination if name is null ', async () => {
        mockMeta = { total: 0, perPage, pages: page, currentPage: 0 };
        jest.spyOn(mockQueryBuilder, 'getMany').mockResolvedValueOnce([]);
        jest.spyOn(mockQueryBuilder, 'getCount').mockResolvedValueOnce(0);
        let { items, meta } = await paginateMany(mockQueryBuilder, mockPaginate);

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });

      it('should return an pagination if set only queryBuilder ', async () => {
        mockMeta = { total: 0, perPage: Number(PAGINATION.PERPAGE), pages: Number(PAGINATION.PAGE), currentPage: 0 };
        jest.spyOn(mockQueryBuilder, 'getMany').mockResolvedValueOnce([]);
        jest.spyOn(mockQueryBuilder, 'getCount').mockResolvedValueOnce(0);
        let { items, meta } = await paginateMany(mockQueryBuilder);

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });
    });

    describe('paginateRawMany()', () => {
      beforeEach(() => {
        mockQueryBuilder = {
          getRawMany: jest.fn(),
          getCount: jest.fn(),
          take: jest.fn().mockReturnThis(),
          skip: jest.fn().mockReturnThis(),
          orderBy: jest.fn().mockReturnThis(),
        } as any;

        mockPaginate = { page, perPage };
        options = {};
      });

      it('should return an pagination if set all values ', async () => {
        mockMeta = { total: 0, perPage, pages: page, currentPage: 0 };
        jest.spyOn(mockQueryBuilder, 'getRawMany').mockResolvedValueOnce([]);
        jest.spyOn(mockQueryBuilder, 'getCount').mockResolvedValueOnce(0);
        let { items, meta } = await paginateRawMany(mockQueryBuilder, mockPaginate, '');

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });

      it('should return an pagination if name is null ', async () => {
        mockMeta = { total: 0, perPage, pages: page, currentPage: 0 };
        jest.spyOn(mockQueryBuilder, 'getRawMany').mockResolvedValueOnce([]);
        jest.spyOn(mockQueryBuilder, 'getCount').mockResolvedValueOnce(0);
        let { items, meta } = await paginateRawMany(mockQueryBuilder, mockPaginate);

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });

      it('should return an pagination if if set only queryBuilder ', async () => {
        mockMeta = { total: 0, perPage: Number(PAGINATION.PERPAGE), pages: Number(PAGINATION.PAGE), currentPage: 0 };
        jest.spyOn(mockQueryBuilder, 'getRawMany').mockResolvedValueOnce([]);
        jest.spyOn(mockQueryBuilder, 'getCount').mockResolvedValueOnce(0);
        let { items, meta } = await paginateRawMany(mockQueryBuilder);

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });
    });

    describe('Paginate()', () => {
      beforeEach(() => {
        mockRepository = {
          findAndCount: jest.fn(),
        } as any;

        mockPaginate = { page, perPage };
        options = {};
      });

      it('should return an pagination if set all values ', async () => {
        mockMeta = { total: 0, perPage, pages: page, currentPage: 0 };
        jest.spyOn(mockRepository, 'findAndCount').mockResolvedValueOnce([[], 0]);

        let { items, meta } = await paginate(mockRepository, options, mockPaginate);

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });

      it('should return an pagination if only there is a value in pagination, page ', async () => {
        mockMeta = { total: 0, perPage: Number(PAGINATION.PERPAGE), pages: page, currentPage: 0 };
        jest.spyOn(mockRepository, 'findAndCount').mockResolvedValueOnce([[], 0]);

        let { items, meta } = await paginate(mockRepository, options, { page });

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });

      it('should return an pagination if only there is a value in pagination, perPage ', async () => {
        mockMeta = { total: 0, perPage, pages: Number(PAGINATION.PAGE), currentPage: 0 };
        jest.spyOn(mockRepository, 'findAndCount').mockResolvedValueOnce([[], 0]);

        let { items, meta } = await paginate(mockRepository, options, { perPage });

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });

      it('should return an pagination if witout pagination ', async () => {
        mockMeta = { total: 0, perPage: Number(PAGINATION.PERPAGE), pages: Number(PAGINATION.PAGE), currentPage: 0 };
        jest.spyOn(mockRepository, 'findAndCount').mockResolvedValueOnce([[], 0]);

        let { items, meta } = await paginate(mockRepository, options);

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });

      it('should return an pagination if set only repository ', async () => {
        mockMeta = { total: 0, perPage: Number(PAGINATION.PERPAGE), pages: Number(PAGINATION.PAGE), currentPage: 0 };
        jest.spyOn(mockRepository, 'findAndCount').mockResolvedValueOnce([[], 0]);

        let { items, meta } = await paginate(mockRepository);

        expect(items).toStrictEqual([]);
        expect(meta).toStrictEqual(mockMeta);
      });
    });
  });
});
