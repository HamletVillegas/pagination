import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, IsOptional, IsPositive } from 'class-validator';

export class PaginationQueryDto {
  @ApiProperty({ type: 'query', description: 'page', required: false })
  @IsNumber()
  @IsPositive()
  @IsOptional()
  @Type(() => Number)
  page: number;

  @ApiProperty({ type: 'query', description: 'perPage', required: false })
  @IsNumber()
  @IsPositive()
  @IsOptional()
  @Type(() => Number)
  perPage: number;
}
