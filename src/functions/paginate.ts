import { Repository, FindManyOptions, FindOptionsWhere, SelectQueryBuilder } from 'typeorm';
import { PAGINATION } from '../constants';
import MetaOptions from '../interfaces/MetaOptions.interface';
import { ObjectLiteral } from '../interfaces/ObjectLiteral.interface';
import PaginationOptions from '../interfaces/PaginationOptions.interface';

function offset({ page, perPage }: PaginationOptions): number {
  return (page - 1) * perPage;
}

function pagination<reposity>({ page, perPage }: PaginationOptions): FindManyOptions<reposity> {
  return { take: perPage, skip: offset({ page, perPage }) };
}

function validation(opt?: PaginationOptions): PaginationOptions {
  const newPage = opt.page || PAGINATION.PAGE;

  const newPrePage = opt.perPage || PAGINATION.PERPAGE;

  return { page: Number(newPage), perPage: Number(newPrePage) };
}
const valuesDefaul = (): PaginationOptions => ({ page: Number(PAGINATION.PAGE), perPage: Number(PAGINATION.PERPAGE) });

const calculationCurrentPage = (totalItems: number, perPage: number): number => Math.ceil(totalItems / perPage);

const createMeta = (totalItems: number, resultPage: PaginationOptions): MetaOptions => ({
  total: totalItems,
  perPage: resultPage.perPage,
  pages: resultPage.page,
  currentPage: calculationCurrentPage(totalItems, resultPage.perPage),
});

const countQuery = async <T>(queryBuilder: SelectQueryBuilder<T>): Promise<number> => queryBuilder.getCount();

const paginateQuery = (queryBuilder: SelectQueryBuilder<ObjectLiteral>, page?: PaginationOptions, name?: string) => {
  const resultPage = page ? validation(page) : valuesDefaul();

  const settings = pagination(resultPage);

  let queryBuilderPagination = queryBuilder.take(settings.take).skip(settings.skip);

  if (name) queryBuilderPagination = queryBuilderPagination.orderBy(`${name}`, PAGINATION.ORDER);

  return { queryBuilderPagination, resultPage };
};

export async function paginate<reposity, entity>(
  repository: Repository<reposity | any>,
  options?: FindOptionsWhere<entity>,
  page?: PaginationOptions,
): Promise<reposity[] | any> {
  const resultPage = page ? validation(page) : valuesDefaul();

  const settings = pagination(resultPage);

  const [items, totalItems] = await repository.findAndCount({
    ...options,
    ...settings,
    order: { createdAt: PAGINATION.ORDER },
  });

  const meta = createMeta(totalItems, resultPage);

  return { items, meta };
}

export async function paginateRawMany<reposity>(
  queryBuilder: SelectQueryBuilder<ObjectLiteral>,
  page?: PaginationOptions,
  name?: string,
): Promise<reposity[] | any> {
  const { queryBuilderPagination, resultPage } = paginateQuery(queryBuilder, page, name);

  const [items, totalItems] = await Promise.all([queryBuilderPagination.getRawMany(), countQuery(queryBuilder)]);

  const meta = createMeta(totalItems, resultPage);

  return { items, meta };
}

export async function paginateMany<reposity>(
  queryBuilder: SelectQueryBuilder<ObjectLiteral>,
  page?: PaginationOptions,
  name?: string,
): Promise<reposity[] | any> {
  const { queryBuilderPagination, resultPage } = paginateQuery(queryBuilder, page, name);

  const [items, totalItems] = await Promise.all([queryBuilderPagination.getMany(), countQuery(queryBuilder)]);

  const meta = createMeta(totalItems, resultPage);

  return { items, meta };
}
