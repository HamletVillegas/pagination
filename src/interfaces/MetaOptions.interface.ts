export default interface MetaOptions {
  total: number;

  perPage: number;

  pages: number;

  currentPage: number;
}
