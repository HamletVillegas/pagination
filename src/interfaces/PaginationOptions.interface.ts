export default interface PaginationOptions {
  /**
   * @default 1
   * The page that is requested
   */
  page?: number;

  /**
   * @default 10
   * the amount of items to be requested per page
   */
  perPage?: number;
}
